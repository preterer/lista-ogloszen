angular.module("app").config(($stateProvider, $urlRouterProvider) => {
  $stateProvider.state("list", {
    url: "/",
    resolve: {
      postings: (postingResource) => {
        return postingResource.query().$promise;
      }
    },
    views: {
      "container": {
        templateUrl: "list.html",
        controller: "listController"
      }
    }
  }).state("form", {
    url: "/form",
    views: {
      "container": {
        templateUrl: "form.html",
        controller: "formController"
      }
    }
  });
});
