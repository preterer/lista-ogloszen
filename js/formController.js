angular.module("app").controller("formController",
  function($scope, $state, postingResource) {
    $scope.posting = {};
    $scope.postalCodeRegex = /^\d\d-\d\d\d$/;

    $scope.savePosting = (posting) => {
      postingResource.save(posting).$promise.then((response) => {
        $state.go("list");
      }, (error) => {
        $scope.error = error;
      });
    };

    $scope.salaryTooWide = (posting) => (posting.salaryMin * 2) < posting.salaryMax;
  });
