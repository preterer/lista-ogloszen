angular.module("app").directive("validateMaxSalary", () => ({
  require: '?ngModel',
  restrict: "A",
  scope: {
    minSalary: "=validateMaxSalary"
  },
  link: (scope, elm, attrs, ctrl) => {
    ctrl.$validators.maxSalary = (modelValue) => {
      return modelValue <= (scope.minSalary * 2);
    }
  }
}));
